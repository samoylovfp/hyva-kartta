# Hyvä Kartta

This project is inspired by the openstreetmaps project.
The goal is to have a lightweight performant mapping app that
downloads the mapping data ahead of time, allowing drawing tiles and
building navigation routes offline.
