pub mod coords;

use std::{
    collections::HashMap,
    io::{Read, Write},
};

use bincode::Options;
use coords::{GeoCoord, PicMercator};
pub use d3_geo_rs::{projection::mercator::Mercator, Transform};
use delta_encoding::{DeltaDecoderExt, DeltaEncoderExt};
pub use geo_types::Coord;
pub use h3o::{CellIndex, LatLng, Resolution};
use itertools::{izip, Itertools};
use log::{debug, trace};
use lz4_flex::frame::{FrameDecoder, FrameEncoder};
use serde::{Deserialize, Serialize};
use size_of::SizeOf;
pub use tiny_skia::{Color, Pixmap};
use tiny_skia::{Paint, PathBuilder, Stroke, Transform as SkiaTransform};

#[derive(Debug, Clone)]
pub struct Path {
    pub points: Vec<GeoCoord>,
    // tags: HashMap<String, String>,
}

#[derive(Serialize, Deserialize, SizeOf)]
pub struct ZanaDenseData {
    pub nodes: ZanaDenseNodes,
    pub paths: ZanaDensePaths,
    pub string_table: HashMap<String, u64>,
}

#[derive(Serialize, Deserialize, SizeOf)]
pub struct ZanaDenseNodes {
    pub dids: Vec<i64>,
    pub dlats: Vec<i32>,
    pub dlons: Vec<i32>,
}

#[derive(Debug, SizeOf)]
pub enum ZanaObj {
    Node(ZanaNode),
    Path(ZanaPath),
}
#[derive(Debug, SizeOf)]
pub struct ZanaNode {
    pub id: i64,
    pub coords: GeoCoord,
}

#[derive(Debug, SizeOf)]
pub struct ZanaPath {
    pub nodes: Vec<i64>,
    pub tags: Vec<(u64, u64)>,
}

#[derive(Debug, Serialize, Deserialize, SizeOf)]
pub struct ZanaDensePaths {
    pub dids: Vec<i64>,
    pub dnodes: Vec<Vec<i64>>,
    /// (key_i, val_i)*, 0
    pub tags: Vec<u64>,
}

/// useful for varint encoding
#[derive(Debug, Serialize, Deserialize)]
pub struct RelativePath {
    relative_points: Vec<(i32, i32)>,
}

pub fn draw_tile(
    pixmap: &mut Pixmap,
    data: impl Read,
    (min_x, max_x, min_y, max_y): (f64, f64, f64, f64),
) {
    let (string_table, zana_data) = read_zana_data(data);

    let node_id_hashmap: HashMap<_, _> = zana_data
        .iter()
        .filter_map(|o| match o {
            ZanaObj::Node(n) => Some((n.id, (n.coords.clone()))),
            _ => None,
        })
        .collect();

    let find_tag = |s: &str| string_table.get(s).copied().unwrap_or(0);

    let building_tag = find_tag("building");
    let power_tag = find_tag("power");
    let highways_tag = find_tag("highway");

    let building_style = PaintStyle::new((20, 100, 20, 50), 0.1);
    let highway_style = PaintStyle::new((255, 150, 20, 5), 0.1);
    let power_style = PaintStyle::new((0, 100, 255, 150), 1.0);
    let _default = PaintStyle::new((5, 5, 5, 0), 0.1);

    let x_span = max_x - min_x;
    let y_span = max_y - min_y;

    let x_size = pixmap.width();
    let y_size = (x_size as f64 / x_span * y_span) as u32;

    let x_scale = x_size as f64 / x_span;
    let y_scale = y_size as f64 / y_span;

    fn has_tag(p: &ZanaPath, tag: u64) -> bool {
        p.tags.iter().any(|(k, _)| *k == tag)
    }

    for obj in &zana_data {
        match obj {
            ZanaObj::Node(_) => {}
            ZanaObj::Path(p) => {
                let mut style = None;

                if has_tag(p, building_tag) {
                    style = Some(&building_style);
                } else if has_tag(p, power_tag) {
                    style = Some(&power_style);
                } else if has_tag(p, highways_tag) {
                    style = Some(&highway_style);
                }
                if let Some(s) = style {
                    draw_path(
                        pixmap,
                        p,
                        &node_id_hashmap,
                        (min_x, min_y),
                        (x_scale, y_scale),
                        s,
                    )
                }
            }
        }
    }
}

struct PaintStyle<'paint> {
    paint: Paint<'paint>,
    stroke: Stroke,
}

impl PaintStyle<'_> {
    fn new(color: (u8, u8, u8, u8), width: f32) -> Self {
        let mut paint = Paint::default();
        let mut stroke = Stroke::default();
        paint.set_color_rgba8(color.0, color.1, color.2, color.3);
        stroke.width = width;
        Self { paint, stroke }
    }
}

fn draw_path(
    pixmap: &mut Pixmap,
    p: &ZanaPath,
    node_id_hashmap: &HashMap<i64, GeoCoord>,
    offset: (f64, f64),
    scale: (f64, f64),
    PaintStyle { paint, stroke }: &PaintStyle,
) {
    let offset_and_scale = |x: f64, y: f64| ((x - offset.0) * scale.0, (y - offset.1) * scale.1);
    let mut pb = PathBuilder::new();
    let node_coords = p
        .nodes
        .iter()
        .filter_map(|n| node_id_hashmap.get(n))
        .collect_vec();
    if node_coords.len() > 1 {
        let PicMercator { x, y } = node_coords[0].project();
        let (x, y) = offset_and_scale(x, y);
        pb.move_to(x as f32, y as f32);
    }
    for node in &node_coords[1..] {
        let PicMercator { x, y } = node.project();
        trace!("mercator: {x}:{y}");
        let (x, y) = offset_and_scale(x, y);
        pb.line_to(x as f32, y as f32);
    }
    if let Some(p) = pb.finish() {
        trace!("{p:?}");
        pixmap.stroke_path(&p, paint, stroke, SkiaTransform::identity(), None);
    }
}

pub fn read_zana_data(r: impl Read) -> (HashMap<String, u64>, Vec<ZanaObj>) {
    let mut result = vec![];
    let bufreader = FrameDecoder::new(r);
    let data: ZanaDenseData = bincode::DefaultOptions::new()
        .deserialize_from(bufreader)
        .unwrap();
    debug!("Dense zana data takes up {:#?}", data.size_of());
    let ZanaDenseData {
        nodes,
        paths,
        string_table,
    } = data;

    // nodes
    let ids = nodes.dids.iter().copied().original();
    let lats = nodes.dlats.iter().copied().original();
    let lons = nodes.dlons.iter().copied().original();

    for (id, lat, lon) in izip!(ids, lats, lons) {
        result.push(ZanaObj::Node(ZanaNode {
            id,
            coords: GeoCoord {
                decimicro_lat: lat,
                decimicro_lon: lon,
            },
        }));
    }

    // paths
    let path_node_ids = paths
        .dnodes
        .into_iter()
        .map(|dnodes| dnodes.into_iter().original().collect_vec());
    let path_tags = paths.tags.split(|t| *t == 0);

    for (node_ids, tags) in izip!(path_node_ids, path_tags) {
        result.push(ZanaObj::Path(ZanaPath {
            nodes: node_ids,
            tags: tags.chunks_exact(2).map(|c| (c[0], c[1])).collect_vec(),
        }))
    }
    debug!(
        "Undensified zana data takes {:#?}, string table takes {:#?}",
        result.size_of(),
        string_table.size_of()
    );
    (string_table, result)
}

#[derive(Default, Clone)]
pub struct StringTable {
    map: HashMap<String, u64>,
}

impl StringTable {
    pub fn new(map: HashMap<String, u64>) -> Self {
        StringTable { map }
    }
    pub fn intern(&mut self, s: &str) -> u64 {
        let next_idx = self.map.len() as u64 + 1;
        match self.map.get(s) {
            Some(n) => *n,
            None => {
                self.map.insert(s.to_string(), next_idx);
                next_idx
            }
        }
    }
    pub fn inverse(self) -> HashMap<u64, String> {
        self.map.into_iter().map(|(k, v)| (v, k)).collect()
    }

    pub fn diff(self, other: StringTable) -> impl Iterator<Item = (String, u64)> {
        self.map
            .into_iter()
            .filter(move |(k, _v)| !other.map.contains_key(k))
    }

    pub fn get_map(self) -> HashMap<String, u64> {
        self.map
    }
}

pub fn write_zana_data(
    nodes: Vec<ZanaNode>,
    paths: Vec<ZanaPath>,
    lookup_table: &HashMap<u64, String>,
    f: impl Write,
) {
    let mut output_string_table = StringTable::default();
    // let dids = paths.iter().map(|w| w.id).deltas().collect_vec();
    let dnodes = paths
        .iter()
        .map(|w| w.nodes.iter().copied().deltas().collect_vec())
        .collect_vec();
    let mut tags = vec![];

    let mut node_ids = vec![];
    let mut node_lats = vec![];
    let mut node_lons = vec![];

    for node in nodes {
        node_ids.push(node.id);
        node_lats.push(node.coords.decimicro_lat);
        node_lons.push(node.coords.decimicro_lon);
    }

    let dense_nodes = ZanaDenseNodes {
        dids: node_ids.into_iter().deltas().collect(),
        dlats: node_lats.into_iter().deltas().collect(),
        dlons: node_lons.into_iter().deltas().collect(),
    };

    for w in paths.iter() {
        for (key, value) in w.tags.iter() {
            tags.push(output_string_table.intern(&lookup_table[key]));
            tags.push(output_string_table.intern(&lookup_table[value]));
        }
        tags.push(0)
    }
    // remove last 0
    if !tags.is_empty() {
        tags.pop();
    }

    let f = FrameEncoder::new(f).auto_finish();
    bincode::DefaultOptions::new()
        .serialize_into(
            f,
            &ZanaDenseData {
                nodes: dense_nodes,
                paths: ZanaDensePaths {
                    dids: vec![],
                    dnodes,
                    tags,
                },
                string_table: output_string_table.map,
            },
        )
        .unwrap();
}

#[cfg(test)]
mod tests {

    use itertools::Itertools;

    use crate::{
        coords::GeoCoord, read_zana_data, write_zana_data, StringTable, ZanaNode, ZanaPath,
    };

    #[test]
    fn smoke_test_string_table() {
        let mut t = StringTable::default();
        assert_eq!(t.intern("a"), 1);
        assert_eq!(t.intern("b"), 2);
        assert_eq!(t.intern("a"), 1);

        assert_eq!(
            t.map,
            [("a".into(), 1), ("b".into(), 2)].into_iter().collect()
        )
    }

    #[test]
    fn smoke_test_read_write() {
        let st = [("map", 1), ("q3dm5", 2)]
            .into_iter()
            .map(|(s, id)| (s.into(), id))
            .collect();

        let n = |i| ZanaNode {
            id: i,
            coords: GeoCoord {
                decimicro_lat: 5 + (i as i32),
                decimicro_lon: 6,
            },
        };
        let mut output = Vec::new();
        write_zana_data(
            vec![n(1), n(2), n(3)],
            vec![ZanaPath {
                nodes: vec![1, 2, 3],
                tags: vec![(1, 2)],
            }],
            &StringTable::new(st).inverse(),
            &mut output,
        );
        let (string_table, objects) = read_zana_data(std::io::Cursor::new(output));

        let string_table = string_table
            .into_iter()
            .map(|(k, v)| format!("{k}:{v}"))
            .sorted()
            .collect_vec();

        insta::assert_debug_snapshot!(string_table, @r###"
        [
            "map:1",
            "q3dm5:2",
        ]
        "###);
        insta::assert_debug_snapshot!(objects, @r###"
        [
            Node(
                ZanaNode {
                    id: 1,
                    coords: GeoCoord {
                        decimicro_lat: 6,
                        decimicro_lon: 6,
                    },
                },
            ),
            Node(
                ZanaNode {
                    id: 2,
                    coords: GeoCoord {
                        decimicro_lat: 7,
                        decimicro_lon: 6,
                    },
                },
            ),
            Node(
                ZanaNode {
                    id: 3,
                    coords: GeoCoord {
                        decimicro_lat: 8,
                        decimicro_lon: 6,
                    },
                },
            ),
            Path(
                ZanaPath {
                    nodes: [
                        1,
                        2,
                        3,
                    ],
                    tags: [
                        (
                            1,
                            2,
                        ),
                    ],
                },
            ),
        ]
        "###);
    }
}
